#!/usr/bin/python

from histg import cleardict
from histg import histogram
from histg import missing

"""
FILE letters.py

OUTPUT

zzz is missing letters abcdefghijklmnopqrstuvwxy
subdermatoglyphic is missing letters fjknqvwx
the quick brown fox jumps over the lazy dog uses all the letters
"""

if __name__ == "__main__":
    test_miss = ["zzz","subdermatoglyphic","the quick brown fox jumps over the lazy dog"] 
    d = dict()
    l = None
    
    for item in test_miss:
        cleardict(d)
        histogram(item,d)
        l = missing(d)
        if l == '':
            print("{0} uses all the letters".format(item))
            continue
        print("{0} is missing letters {1}".format(item,l))
