#!/usr/bin/python

from histg import cleardict
from histg import histogram
from histg import has_duplicates

"""
FILE duplicates.py

OUTPUT

zzz has duplicates? True
dog has duplicates? False
bookkeeper has duplicates? True
subdermatoglyphic has duplicates? False
subdermatoglyphics has duplicates? True
"""

if __name__ == "__main__":
    test_dups = ["zzz","dog","bookkeeper","subdermatoglyphic","subdermatoglyphics"]
    d = dict()
    check = None
    
    for item in test_dups:
        cleardict(d)
        histogram(item,d)
        check = has_duplicates(d)
        print("{0} has duplicates? {1}".format(item,check))
