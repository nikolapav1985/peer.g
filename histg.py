#!/usr/bin/python

"""
FILE histg.py

a number of dictionary functions
"""

ALPHABET='abcdefghijklmnopqrstuvwxyz'

def cleardict(dct):
    """
        method cleardict

        set all dictionary keys to zero

        parameters

        dct(dictionary) a disctionary that needs to be cleared
    """
    for key in dct:
        dct[key]=0

def histogram(s,dct):
    """
        method histogram

        produce histogram (count) of letters in a string

        parameters

        s(string) a string to use
        dct(dictionary) a dictionary to use for counting
    """
    for c in s:
        if c not in dct:
            dct[c]=1
        else:
            dct[c]+=1

def has_duplicates(dct):
    """
        method has_duplicates

        check if a string has duplicates

        parameters

        dct(dictionary) a dictionary that has character count

        return

        check(boolean) True if string has duplicates (dictionary entry count more than one) False in other case
    """
    for key in dct:
        if dct[key]>1:
            return True
    return False

def missing(dct):
    """
        method missing

        check for missing letters

        parameters

        dct(dictionary) a dictionary that contains letter count

        return

        l(string) a string of missing letters
    """
    l = ''
    for c in ALPHABET:
        if c not in dct:
            l+=c
    return l
