Assignment unit 7
-----------------

- histg.py (dictionary methods)
- duplicates.py (check duplicates)
- letters.py (check missing letters)

Output (duplicates)
-------------------

- zzz has duplicates? True
- dog has duplicates? False
- bookkeeper has duplicates? True
- subdermatoglyphic has duplicates? False
- subdermatoglyphics has duplicates? True

Output (letters)
----------------

- zzz is missing letters abcdefghijklmnopqrstuvwxy
- subdermatoglyphic is missing letters fjknqvwx
- the quick brown fox jumps over the lazy dog uses all the letters

Test environment
----------------

os lubuntu 16.04 lts 4.13.0 kernel version 2.7.12 python version
